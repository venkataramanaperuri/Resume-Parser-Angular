import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CanditatesDetailService } from '../canditates-detail.service';
import { Observable } from 'rxjs';
import { JobsService } from '../jobs.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

  elements:any = [];
  profileForm:any = FormGroup;

  name: string = '';
  files: any;
  pdfFile: any;
  result: any;

  candidates:any=[];
  displayedColumns: string[] = ['name','jobDescription','filename','match'];
  dataSource!:MatTableDataSource<any>;

  getTheInput(name: string){
    this.name = name;
  }

  getTheFile(event: any){
    this.files = event.target.files[0];
    console.log('file', this.files);
  }

  constructor(private http: HttpClient, private jobsService: JobsService, private router: Router) { }

  ngOnInit() {
    
    this.elements = this.jobsService.getJobDescriptions().subscribe(
      (data) => { 
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.elements = data;
      }
    )
  }

  onSubmit(){
    console.log('button pressed');
    let formData = new FormData();
    formData.set("name", this.name);
    formData.set("jd_file", this.files);

    this.http.post('http://localhost:5000/uploads_jd', formData).subscribe(
      resp => {
        this.elements = resp;
        console.log(this.elements)
      }
    )
  }

  onMatch(){
    console.log('onMatch pressed');
    this.router.navigate(['/job-match'])
  }

  applyFilter(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
