import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import { CanditatesDetailService } from '../canditates-detail.service';

@Component({
  selector: 'app-candidates-detail',
  templateUrl: './candidates-detail.component.html',
  styleUrls: ['./candidates-detail.component.css']
})
export class CandidatesDetailComponent implements OnInit  {

  candidates:any=[];
  displayedColumns: string[] = ['name', 'Contact', 'email', 'skills'];
  dataSource!:MatTableDataSource<any>;
  
  constructor(private http : HttpClient, private candidateService : CanditatesDetailService) { }

  ngOnInit(): void {
    
    this.candidates = this.candidateService.getCandidate().subscribe(
      (data) => { 
        console.log(data)
        this.dataSource = new MatTableDataSource(data);
        this.candidates = data;
      }
    )
          
  }

  applyFilter(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}

