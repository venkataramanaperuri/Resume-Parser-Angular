import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

export interface PeriodicElement {
  name: string;
  email: string;
  contact: number;
  skills: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {email: "alok1198ranjan@gmail.com", name: 'Alok Ranjan', contact: 8123289303, skills: 'Java, UI, UX'},
  {email: "ankroy7@gmail.com", name: 'Ankur Roy', contact: 8123986588, skills: 'Java, Microservices'},
  {email: "pankajkumar150@gmail.com", name: 'Pankaj Kumar', contact: 7760035677, skills: 'Selenium, Automation, Microservices'},
];

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})


export class TableComponent implements OnInit {

  displayedColumns: string[] = ['email', 'name', 'contact', 'skills'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);


  @ViewChild(MatSort) matSort! : MatSort;

  constructor() { }

  ngOnInit(): void {
    this.dataSource.sort = this.matSort;
  }

  applyFilter(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
