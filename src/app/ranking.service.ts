import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RankingService {

  constructor(private http: HttpClient) { }

  private baseUrl = 'http://localhost:5000/ranking_fun';

  getRankedCandidates(): Observable<any[]>{
    
    return this.http.get<any[]>(`${this.baseUrl}`);
  
  }

}
